package com.rony;

import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;


public class ChatController {

    @FXML
    TextField messageToSend;

    @FXML
    TextArea chatMessages;

    @FXML
    ListView<String> userList;

    static boolean userIsConnected = true;
    private final InetAddress group = InetAddress.getByName("239.0.0.0");
    private final int port = 1234;
    private final MulticastSocket socket = new MulticastSocket(port);
    long timeSinceLastRequest;

    public ChatController() throws IOException {
    }

    public void initialize() throws IOException {

        ObservableList<String> users = userList.getItems();
        users.add(User.getUsername());
        userList.setItems(users);

        final int MAX_LEN = 1000;
        User.setUsername(User.getUsername());

        // Since we are deploying
        socket.setTimeToLive(0);
        //this on localhost only (For a subnet set it as 1)
        socket.joinGroup(group);

        Task task = new Task() {
            @Override
            protected Void call() {

                while (true) {
                    byte[] buffer = new byte[MAX_LEN];
                    DatagramPacket datagram = new
                            DatagramPacket(buffer, buffer.length, group, port);

                    try {
                        socket.receive(datagram);
                        Platform.runLater(() -> {
                            String message;
                            message = new
                                    String(buffer, 0, datagram.getLength(), StandardCharsets.UTF_8);

                            if (message.contains("/*KEEPALIVE*/")) {
                                String keepAliveUser = message.substring(13);
                                if (!users.contains(keepAliveUser)) {

                                    users.add(keepAliveUser);
                                    userList.setItems(users);
                                }
                            } else if (message.contains("/*DESTROY*/")) {
                                String destroyUser = message.substring(11);
                                if (destroyUser != User.getUsername()) {
                                    users.remove(destroyUser);
                                    userList.setItems(users);
                                }
                            } else if (message.startsWith("/*INCPM*/")) {
                                String[] parts = message.split("\\|");
                                if (parts.length > 1 && parts[1].equals(User.getUsername())) {
                                    String currentLog = chatMessages.getText();
                                    chatMessages.setText(currentLog + parts[2] + "\n");
                                }
                            } else {
                                String currentLog = chatMessages.getText();
                                chatMessages.setText(currentLog + message + "\n");
                            }


                        });
                    } catch (IOException e) {
                        System.out.println("Socket closed!");
                    }
                }
            }
        };

        Thread isConnectedListener = new Thread(() -> {
            while (true) {
                if (!userIsConnected) {
                    break;
                } else {
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    String keepAlive = "/*KEEPALIVE*/" + User.getUsername();
                    byte[] buffer = keepAlive.getBytes();
                    DatagramPacket datagram = new
                            DatagramPacket(buffer, buffer.length, group, port);
                    try {
                        socket.send(datagram);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        isConnectedListener.setDaemon(true);
        isConnectedListener.start();


        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();
    }

    public void sendMessage(ActionEvent actionEvent) throws IOException {
        if (!messageToSend.getText().trim().equals("")) {

            String messageBody;

            if (messageToSend.getText().trim().startsWith("/r")) {
                String[] messageArray = messageToSend.getText().trim().split(" ");
                StringBuilder privateMessageBody = new StringBuilder();
                for (int i = 2; i < messageArray.length; i++) {
                    privateMessageBody.append(messageArray[i]);
                }
                String currentLog = chatMessages.getText();
                chatMessages.setText(currentLog + User.getUsername() +": " +  privateMessageBody + "\n");
                messageToSend.setText(User.getUsername() + ": " + messageToSend.getText().trim());

                messageBody = String.format("/*INCPM*/|%s|%s: %s", messageArray[1], User.getUsername(), privateMessageBody.toString());

            } else {
                messageBody = User.getUsername() + ": " + messageToSend.getText().trim();
            }

            messageToSend.setText("");
            byte[] buffer = messageBody.getBytes();
            DatagramPacket datagram = new
                    DatagramPacket(buffer, buffer.length, group, port);
            socket.send(datagram);
        }
    }
}
