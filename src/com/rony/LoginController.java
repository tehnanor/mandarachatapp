package com.rony;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;


import java.io.IOException;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.Scanner;

public class LoginController {
    @FXML
    private TextField chosenUsername;

    @FXML
    private Text errorMessage;

    public void handleLoginButton(ActionEvent actionEvent) throws IOException {
        if(chosenUsername.getText().trim().equals("")){
            errorMessage.setText("Please enter a non-empty username.");
        } else {
            User.setUsername(chosenUsername.getText().trim());
            switchToNewScene("chatRoom.fxml");
        }
    }

    private void switchToNewScene(String fxmlLocation) throws IOException {
        Parent parent = FXMLLoader.load(Main.class.getResource(fxmlLocation), null, new JavaFXBuilderFactory());
        Stage stage = Main.getStage();
        stage.getScene().setRoot(parent);
        stage.sizeToScene();
    }
}
