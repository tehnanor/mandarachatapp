package com.rony;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class Main extends Application {

    private static Stage stage;
    static boolean userIsConnected = true;
    private final InetAddress group = InetAddress.getByName("239.0.0.0");
    private final int port = 1234;
    private final MulticastSocket socket = new MulticastSocket(port);
    long timeSinceLastRequest;

    public Main() throws IOException {
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        this.stage = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
        primaryStage.setTitle("Mandara Chat Application");
        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }

    static public Stage getStage() {
        return stage;
    }

    @Override
    public void stop(){
        ChatController.userIsConnected = false;
        String destroyUser = "/*DESTROY*/" + User.getUsername();
        byte[] buffer = destroyUser.getBytes();
        DatagramPacket datagram = new
                DatagramPacket(buffer, buffer.length, group, port);
        try {
            socket.send(datagram);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
